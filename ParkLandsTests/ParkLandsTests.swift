//
//  ParkLandsTests.swift
//  ParkLandsTests
//
//  Created by Apptivty Lab on 30/03/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import XCTest
@testable import ParkLands

class ParkLandsTests: XCTestCase {
    
    var session: URLSession!
    var baseURL: URL!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let config: URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "C38F1069-3616-0E1D-FF21-6F6B53B70400",
                                        "secret-key": "9AB71490-599E-C860-FFA6-03AC0B43CD00",
                                        "application-type": "REST",
                                        "Content-Type": "application/json"]
        
        self.session = URLSession(configuration: config)
        self.baseURL = URL(string: "https://api.backendless.com/v1/")!
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func generateBoundary() -> String {
        return NSUUID().uuidString
    }
    
    func createBody(boundary: String,
                    data: Data,
                    type: String,
                    fileName: String) -> Data {
        let body: NSMutableData = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        body.append(boundaryPrefix.data(using: .utf8)!)
        body.append("Content-Disposition: form-data; name=\"file\"\(fileName)\"\r\n".data(using: .utf8)!)
        body.append("Content-Type: \(type)\r\n\r\n".data(using: .utf8)!)
        body.append(data)
        body.append("--".appending(boundary.appending("--")).data(using: .utf8)!)
        
        return body as Data
    }
    
    
}
