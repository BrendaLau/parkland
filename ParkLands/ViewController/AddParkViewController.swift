//
//  AddParkViewController.swift
//  ParkLands
//
//  Created by Apptivty Lab on 06/04/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import UIKit
import MBProgressHUD

class AddParkViewController: UIViewController {

    @IBOutlet weak var parkImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var ratingPickerView: UIPickerView!
    @IBOutlet weak var addParkVCScrollView: UIScrollView!
    
    @IBOutlet var tapGestureRecongnizer: UITapGestureRecognizer!
    let libraryGestureRecog: UITapGestureRecognizer = UITapGestureRecognizer()
    let photoPicker: UIImagePickerController =  UIImagePickerController()
    
    let ratings: [String] = ["One","Two","Three","Four","Five"]
    var selectedRating: Int = 1
    
    var park: Park!
    var currentPhoto: UIImage? //For editing the photo from park. Avoids uploading a duplicate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        libraryGestureRecog.addTarget(self, action: #selector(AddParkViewController.loadPhoto))
        parkImageView.addGestureRecognizer(libraryGestureRecog)
        parkImageView.isUserInteractionEnabled = true
        
        photoPicker.delegate = self
        
        //editing an existing park
        if let park: Park = park {
            navigationItem.title = "Edit Park"
            nameTextField.text = park.name
            parkImageView.image = park.photo
            currentPhoto = park.photo
            locationTextField.text = park.location
            descriptionTextField.text = park.description
            //ratingPickerView.selectedRow(inComponent: park.rating)
            ratingPickerView.selectRow(park.rating - 1, inComponent: 0, animated: false)
            selectedRating = park.rating - 1
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.parkImageView.removeGestureRecognizer(libraryGestureRecog)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "unwindToParkListVC" {
            if let destVC: ParkListViewController = segue.destination as? ParkListViewController {

                if let park: Park = park {
                    destVC.parks.append(park)
                }
            }
        }
        
        if segue.identifier == "unwindToParkDetailVC" {
            if let destVC: ParkDetailViewController = segue.destination as? ParkDetailViewController {
                destVC.park = park
            }
        }
    }
    
    @IBAction func saveParkButton(_ sender: Any) {
        //check if values have been entered
        let rating: Int = selectedRating + 1
        guard
            !(nameTextField.text?.isEmpty ?? false),
            let description: String = descriptionTextField.text,
            let location: String = locationTextField.text,
            
            let photo: UIImage = parkImageView.image,

            self.parkImageView.image != UIImage(named: "defaultPhoto"),
            description.isEmpty == false,
            location.isEmpty == false
        else {
            let alert = UIAlertController(title: "Required Fields", message: "Please enter all details of Park", preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
            
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //editing the Park
        if navigationItem.title == "Edit Park" {
            park.description = description
            park.location = location
            park.rating = rating
            park.photo = photo
            
            if let name: String = nameTextField.text {
                park.name = name
            }

            editPark()
            
        //Adding a new Park
        }else if navigationItem.title == "Add Park" {
            self.park = Park(name: nameTextField.text!, description: description, rating: rating, location: location, photo: photo)
            addPark()
        }
    }
 
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func loadPhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func hideMBProgressHUD(alert: UIAlertController) {
        
        let deadlineTime = DispatchTime.now() + .seconds(1)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
            MBProgressHUD.hide(for: self.view, animated: true)
            
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    func editPark() {
        
        if parkImageView.image != currentPhoto {
            //upload a new image
            ParkLoader.shareLoader.createImageOnServer(park: park!) { (fileURL, error, fileString) in
                
                self.park.photoURL = URL(string: fileString!)
                
                if let fileString: String = fileString {
                    self.park.photoURL = URL(string: fileString)
                }
                
                ParkLoader.shareLoader.updateParkOnServer(park: self.park, completionBlock: { (true, nil) in
                    let alert = self.okAlertController(title: "Park Updated!", message: "Park \(self.park.name) has been updated!", unwindTo: "parkDetailVC")
                    self.hideMBProgressHUD(alert: alert)
                })
            }
        }else {
            //only update the data
            ParkLoader.shareLoader.updateParkOnServer(park: park, completionBlock: { (true, nil) in
                let alert = self.okAlertController(title: "Park Updated!", message: "Park \(self.park.name) has been updated!", unwindTo: "parkDetailVC")
                self.hideMBProgressHUD(alert: alert)
            })
        }
    }
    
    func addPark() {
        let alert = okAlertController(title: "Park Added!", message: "Park \(park.name) has been added to the list!", unwindTo: "parkListVC")
        
        if let park: Park = park {
            ParkLoader.shareLoader.createImageOnServer(park: park) { (fileURL, error, fileString) in
                
                if let fileString: String = fileString {
                    self.park.photoURL = URL(string: fileString)
                }
                
                ParkLoader.shareLoader.createParkOnServer(park: self.park) { (true, nil) in
                    self.hideMBProgressHUD(alert: alert)
                }
            }
        }
    }
    
    func okAlertController(title: String, message: String, unwindTo: String ) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction: UIAlertAction = UIAlertAction(title: "Okay", style: .default, handler: { (result) in
            
            //unwind to ParkListVC
            
            if unwindTo == "parkDetailVC" {
                self.performSegue(withIdentifier: "unwindToParkDetailVC", sender: self)
            }else {
                self.performSegue(withIdentifier: "unwindToParkListVC", sender: self)
            }
            alert.dismiss(animated: true, completion: nil)
            
        })
        alert.addAction(okAction)
        return alert
    }

}

// MARK: - UIPickerView Extensions
extension AddParkViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return ratings[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedRating = row
    }
}

extension AddParkViewController: UIPickerViewDataSource {
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return ratings.count
    }
}

extension AddParkViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedPhoto: UIImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            parkImageView.contentMode = .scaleAspectFit
            parkImageView.image = pickedPhoto
        }
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddParkViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            self.locationTextField.becomeFirstResponder()
            self.addParkVCScrollView.isScrollEnabled = true
        }
        if textField == locationTextField {
            descriptionTextField.becomeFirstResponder()
            addParkVCScrollView.setContentOffset(CGPoint(x: 0, y: 10), animated: true)
        }
        if textField == descriptionTextField {
            textField.resignFirstResponder()
            addParkVCScrollView.setContentOffset(CGPoint(x: 0, y: -65), animated: true)
        }

        return true
    }
}
