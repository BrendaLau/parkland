//
//  ParkTableViewCell.swift
//  ParkLands
//
//  Created by Apptivty Lab on 06/04/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import UIKit

class ParkTableViewCell: UITableViewCell {

    @IBOutlet weak var parkImageView: UIImageView!
    @IBOutlet weak var parkNameLabel: UILabel!
    
    var loadImageTask: URLSessionDataTask?
    
    var park: Park! {
        didSet {
            updateDisplay()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateDisplay() {
        self.parkNameLabel.text = self.park.name
        
        if let photo: UIImage = self.park.photo {
            self.parkImageView.image = photo
        }else if let photoURL = self.park.photoURL {
            self.loadImageTask?.cancel()
            self.loadImageTask = ParkLoader.shareLoader.loadImageFromServer(imageURL: photoURL, completionBlock: { (image, error) in
                
                self.park.photo = image
                self.parkImageView.image = self.park.photo
                
            })
            self.loadImageTask?.resume()
        }
    }

}
