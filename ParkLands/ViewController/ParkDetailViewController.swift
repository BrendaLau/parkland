//
//  ParkDetailViewController.swift
//  ParkLands
//
//  Created by Apptivty Lab on 30/03/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import UIKit

class ParkDetailViewController: UIViewController {

    @IBOutlet weak var parkImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    var park:Park!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupParkDetails()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "showEditParkVC" {
            
            if let navigationVC: UINavigationController = segue.destination as? UINavigationController {
                
                if let destVC: AddParkViewController = navigationVC.topViewController as? AddParkViewController {
                    destVC.park = park
                }
            }
        }
    }
    
    func setupParkDetails() {
        self.title = park.name
        self.parkImageView.image = park.photo
        self.locationLabel.text  = park.location
        self.descriptionLabel.text = park.description
        self.ratingLabel.text = String(describing: park.rating)
    }
    
    @IBAction func editParkButton(_ sender: Any) {
        self.performSegue(withIdentifier: "showEditParkVC", sender: nil)
    }
    
    @IBAction func unwindToParkDetailVC(sender: UIStoryboardSegue) {
        setupParkDetails()
    }
}
