//
//  ParkListViewController.swift
//  ParkLands
//
//  Created by Apptivty Lab on 30/03/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import UIKit
import MBProgressHUD

class ParkListViewController: UIViewController {

    var parks: [Park] = []
    
    @IBOutlet weak var parkSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadParks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let selectedIndexPath: IndexPath = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showAddParkVC" {
            if let _: AddParkViewController = segue.destination as? AddParkViewController {
            }
            
        }else if segue.identifier == "showParkDetailVC" {
            if let destVC: ParkDetailViewController = segue.destination as? ParkDetailViewController {
                destVC.park = sender as? Park
            }
        }
    }
    
    func loadParks() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        ParkLoader.shareLoader.readParksFromServer { (parks, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            if let error: Error = error {
                debugPrint(error)
            }else {
                self.parks = parks
                self.tableView.reloadData()
            }
        }
    }

    @IBAction func addParkButton(_ sender: Any) {
        self.performSegue(withIdentifier: "showAddParkVC", sender: nil)
    }
    
    @IBAction func unwindToParkListVC(segue: UIStoryboardSegue) {
        loadParks()
    }
    
}

extension ParkListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let park: Park = parks[indexPath.row]
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        self.performSegue(withIdentifier: "showParkDetailVC", sender: park)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let alert: UIAlertController = UIAlertController(title: "Park deleted!", message: "Park has been deleted!", preferredStyle: .alert)
            let okAction: UIAlertAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
            alert.addAction(okAction)

            MBProgressHUD.showAdded(to: self.view, animated: true)
            ParkLoader.shareLoader.deleteParkOnServer(park: parks[indexPath.row], completionBlock: { (true, error) in
                
                ParkLoader.shareLoader.deleteImageOnServer(park: self.parks[indexPath.row], completionBlock: { (true, error) in
                    self.parks.remove(at: indexPath.row)
                    
                    let deadlineTime = DispatchTime.now() + .seconds(3)
                    DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.present(alert, animated: true, completion: nil)
                        self.tableView.reloadData()
                    })
                })
            })
        }
    }
    
}

extension ParkListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return parks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ParkTableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ParkTableViewCell)!
        
        cell.park = parks[indexPath.row]
        return cell
    }
}
