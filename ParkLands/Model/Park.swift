//
//  Park.swift
//  ParkLands
//
//  Created by Apptivty Lab on 30/03/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.


import UIKit

class Park {
    
    var name: String
    var location: String
    var rating: Int
    var description: String
    
    var photo: UIImage?
    var objectId: String?
    var photoURL: URL?
    var photoFileName: String? {
        get {
            return self.photoURL?.lastPathComponent
        }
    }
    
    //for creating the park object from the device
    init(name: String, description: String, rating: Int, location: String, photo: UIImage) {
        self.name = name
        self.description = description
        self.rating = rating
        self.location = location
        self.photo = photo
    }
    
    //for fetching the park object from server with photo URL
//    init(objectId: String, name: String, description: String, rating: Int, location: String, photoURL: URL) {
//        self.objectId = objectId
//        self.name = name
//        self.description = description
//        self.rating = rating
//        self.location = location
//        self.photoURL = photoURL
//    }

    
    //for getting the park data from the server
    init?(json: [String: AnyObject]) {
        guard
            let objectId: String = json["objectId"] as? String,
            let name: String = json["name"] as? String,
            let description: String = json["description"] as? String,
            let location: String = json["location"] as? String,
            let rating: Int = json["rating"] as? Int,
            let urlString: String = json["photoURL"] as? String,
            let photoURL: URL = URL(string: urlString)
            else {
                return nil
        }
        
        self.objectId = objectId
        self.name = name
        self.description = description
        self.location = location
        self.rating = rating
        self.photoURL = photoURL
                
//        self.init(objectId: objectId, name: name, description: description, rating: rating, location: location, photoURL: photoURL)
        
    }
    
    func jsonDict() -> [String:Any] {
        let jsonDict: [String: Any] = ["name": self.name,
                                       "description": self.description,
                                       "rating": self.rating,
                                       "location": self.location,
                                       "photoURL": (self.photoURL?.absoluteString ?? "")]
        return jsonDict
    }
}
