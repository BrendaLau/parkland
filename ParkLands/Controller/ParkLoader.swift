//
//  ParkManager.swift
//  ParkLands
//
//  Created by Apptivty Lab on 06/04/2017.
//  Copyright © 2017 Apptivty Lab. All rights reserved.
//

import UIKit

class ParkLoader {
    static let shareLoader: ParkLoader = ParkLoader()
    
    //Manage loading from server
    var session: URLSession
    var baseURL: URL = URL(string: "https://api.backendless.com/v1/")!
    private init() {
        //Initialize
        let config: URLSessionConfiguration = URLSessionConfiguration.default
        config.httpAdditionalHeaders = ["application-id": "C38F1069-3616-0E1D-FF21-6F6B53B70400",
                                        "secret-key": "9AB71490-599E-C860-FFA6-03AC0B43CD00",
                                        "application-type": "REST",
                                        "Content-Type": "application/json"]
        
        self.session = URLSession(configuration: config)
    }
    
    func doSomething(name: String) -> String {
        //do something here
        return String()
    }
    
    //Creates an image on the server
    func createImageOnServer(park: Park, completionBlock: ((_ fileURL: URL?, _ error: Error?, _ fileString: String?) -> Void)?) {
        
        let boundary: String = generateBoundary()
        
        //let nameSpace: String = park.name.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
        let photoNameJPEG: String = "\(boundary).jpeg"
        
        let url: URL = URL(string: "files/\(photoNameJPEG)", relativeTo: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
        
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        if let photo: UIImage = park.photo {
            let imageData: Data = UIImageJPEGRepresentation(photo, 0.6)!
            let mimeType: String = "image/jpeg"
            
            urlRequest.httpBody = createBody(boundary: boundary,
                                             data: imageData,
                                             type: mimeType,
                                             fileName: photoNameJPEG)
            
            let postTask: URLSessionDataTask = self.session.uploadTask(with: urlRequest as URLRequest, from: nil) { (data: Data?, response: URLResponse?, error: Error?) in

                //let httpCode: Int = (response as! HTTPURLResponse).statusCode
                //debugPrint("HTTP: \(httpCode)")
                
                //receive file URL and name
                let json: [String: AnyObject]?
                
                if let data: Data = data {
                    do {
                        json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String: AnyObject]
                        
                        if let fileURLString: String = json?["fileURL"] as? String,
                            let fileURL =  URL(string: fileURLString) {
                            park.photoURL = fileURL
                        }
                        
                        completionBlock?(park.photoURL, nil, park.photoURL?.absoluteString ?? "")
                        
                    } catch let jsonError {
                        completionBlock?(url, jsonError as Error, nil)
                    }
                }
            }
            postTask.resume()

        }
    }
    
    //Read Parks from Network
    func readParksFromServer(completionBlock:((_ parks: [Park], _ error: Error?) -> Void)?) {
        let url: URL = URL(string: "data/Park", relativeTo: self.baseURL)!
        
        var parksFromServer: [Park] = []
        
        let loadParkTask: URLSessionDataTask = self.session.dataTask(with: url) { (data:Data?, response: URLResponse?, error: Error?) in
            //check returned data validity
            
            var json: [String: AnyObject]?
            
            if let data: Data = data {
                do {
                    json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? [String: AnyObject]
                    
                }catch let jsonError {
                    completionBlock?([], jsonError as Error)
                }
                
                //Proces JSON into Park array
                if let resultsArray: [ [String: AnyObject] ] = json?["data"] as? [ [String: AnyObject] ] {
                    for jsonDict in resultsArray {
                        
                        if let park: Park = Park(json: jsonDict) {
                            parksFromServer.append(park)
                        }
                    }
                }
                
                DispatchQueue.main.async(execute: { () -> Void in
                    completionBlock?(parksFromServer, nil)
                })
            }

        }
        loadParkTask.resume()
    }
    
    //Loading images using a URL
    func loadImageFromServer(imageURL: URL, completionBlock:((_ image: UIImage, _ error: Error?) -> Void)?) -> URLSessionDataTask {
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: imageURL)
        urlRequest.cachePolicy = NSURLRequest.CachePolicy.returnCacheDataElseLoad
        
        var imagesFromServer: UIImage?
        
        let task: URLSessionDataTask = self.session.dataTask(with: urlRequest as URLRequest) { (imageData:Data?, response:URLResponse?, error:Error?) in
            
            if let imageData: Data = imageData {
                imagesFromServer = UIImage(data: imageData)
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                if let imagesFromServer: UIImage = imagesFromServer {
                    (completionBlock?(imagesFromServer, nil))!
                }
            })
        }
        return task
    }
    
    func createParkOnServer(park: Park, completionBlock:((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        let url: URL = URL(string: "data/Park", relativeTo: self.baseURL)!
        let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
        urlRequest.httpMethod = "POST"
        
        let jsonDict: [String: Any] = park.jsonDict()
        
        do {
            
            let jsonData: Data = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions(rawValue: 0))
            urlRequest.httpBody = jsonData
            
            let postTask: URLSessionDataTask = self.session.dataTask(with: urlRequest as URLRequest) { (data: Data?, response: URLResponse?, erorr: Error?) in
                
                do {
                    let json: [String: AnyObject] = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions(rawValue: 0)) as! [String: AnyObject]
                    
                    park.objectId = json["objectId"] as? String
                    
                    completionBlock?(true, nil)
                }catch let jsonError {
                    completionBlock?(false, jsonError as Error)
                }
                
            }
            
            postTask.resume()
            
        } catch let jsonError {
            completionBlock?(false, jsonError as Error)
        }

    }
    
    func updateParkOnServer(park: Park, completionBlock:((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        
        if let objectId: String = park.objectId {
            let url: URL = URL(string: "data/Park/\(objectId)", relativeTo: self.baseURL)!
            
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            urlRequest.httpMethod = "PUT"
            
            let jsonDict: [String: Any] = park.jsonDict()
            
            do {
                let jsonData: Data = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions(rawValue: 0))
                
                urlRequest.httpBody = jsonData
                
                let putTask: URLSessionDataTask = self.session.dataTask(with: urlRequest as URLRequest) { (data:Data?, response:URLResponse?, error: Error?) in

//                    if let response: URLResponse = response {
//                        let httpCode: Int = (response as! HTTPURLResponse).statusCode
//                        debugPrint("HTTP \(httpCode)")
//                    }
                    
                    completionBlock?(true, nil)
                }
                putTask.resume()
                
            }catch let jsonError {
                completionBlock?(false, jsonError as Error)
            }
        }
        completionBlock?(false, nil)
    }
    
    func deleteParkOnServer(park: Park, completionBlock:((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        if let objectId: String = park.objectId {
            let url: URL = URL(string: "data/Park/\(objectId)", relativeTo: self.baseURL)!
            
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            urlRequest.httpMethod = "DELETE"
            
            let bodyObject: [String: Any] = ["deletionTime": ""]
            
            do{
                let jsonData: Data = try JSONSerialization.data(withJSONObject: bodyObject, options: JSONSerialization.WritingOptions(rawValue: 0))
                
                urlRequest.httpBody = jsonData
                
                let deleteTask: URLSessionDataTask = self.session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in

                    completionBlock?(true, nil)
                })
                deleteTask.resume()
                
            }catch let jsonError {
                completionBlock?(false, jsonError as Error)
            }
            
        }
    }
    
    func deleteImageOnServer(park: Park, completionBlock:((_ succeeded: Bool, _ error: Error?) -> Void)?) {
        if let photoFileName: String = park.photoFileName {
            let url: URL = URL(string: "files/\(photoFileName)", relativeTo: baseURL)!
            
            let urlRequest: NSMutableURLRequest = NSMutableURLRequest(url: url)
            urlRequest.httpMethod = "DELETE"
            let bodyObject: [String: Any] = [:]
            
            do {
                let jsonData: Data = try JSONSerialization.data(withJSONObject: bodyObject, options: JSONSerialization.WritingOptions(rawValue: 0))
                
                urlRequest.httpBody = jsonData
                
                let deleteFileTask: URLSessionDataTask = self.session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data:Data?, response:URLResponse?, error:Error?) in
                    
                    completionBlock?(true, nil)
                })
                deleteFileTask.resume()
            }catch let jsonError {
                completionBlock?(false, jsonError as Error)
            }
        }
    }
    
    func generateBoundary() -> String {
        return NSUUID().uuidString
    }
    
    func createBody(boundary: String,
                    data: Data,
                    type: String,
                    fileName: String) -> Data {
        let body: NSMutableData = NSMutableData()
        let boundaryPrefix = "--\(boundary)\r\n"
        
        body.append(stringToData(stringToChange: boundaryPrefix))
        body.append(stringToData(stringToChange: "Content-Disposition: form-data; name=\"photoName\";"))
        body.append(stringToData(stringToChange: "filename=\"\(fileName)\"\r\n"))
        body.append(stringToData(stringToChange: "Content-Type: \(type)\r\n\r\n"))
        body.append(data)
        body.append(stringToData(stringToChange: "\r\n"))
        
        return body as Data

    }
    
    func stringToData(stringToChange: String) -> Data {
        return stringToChange.data(using: .utf8)!

    }
}
